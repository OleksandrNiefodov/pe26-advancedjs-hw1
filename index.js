'use sctrict'
/*
Теоретичне питання

Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

- коли ми створєюмо певний клас, ми можемо використовувати його за основу для подальношого наслідування 
як прототип, цей основний клас буде містити основні характеристики притаманні цьому класу, наприклад Машина. У машини будуть характеристики: колір, рік випуску, модель, марка, тощо. 
Тобто, від цього класу ми можемо унаслідувати основні характеристики. Це полегшує розробку та укономить пам'ять.

Для чого потрібно викликати super() у конструкторі класу-нащадка?

- super() використовується для виклику конструктора батьківського класу. Це важливо, тому що конструктор батьківського класу може ініціалізувати змінні екземпляра, які необхідні для роботи класу нащадка.


Завдання

Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
Створіть гетери та сеттери для цих властивостей.
Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.
*/

    class Employee {
        constructor(name, age, salary){
            this._name = name;
            this._age = age;
            this._salary = salary;
        };

        get name(){
            return this._name;
        };

        set name (newName) {
            this._name = newName;
        };

        get age() {
            return this._age;
        };

        set age(newAge) {
            this._age = newAge;
        };

        get salary() {
            return this._salary;
        };

        set salary(newSalary) {
            this._salary = newSalary;
        };
    };




    class Programmer extends Employee {
        
        constructor(name, age, salary, lang){
            super(name, age, salary);
            this.lang = lang;
        };

        get salary() {
            return `${this.name}: ${super.salary * 3}$`;
        };
    };
   
    const employeeOleksandr = new Programmer('Oleksandr', 38, 10000, 'JavaScript');
    const employeeVasyl = new Programmer('Vasyl', 33, 15000, 'C#');
    const employeeRoman = new Programmer('Roman', 29, 7000, 'nodeJS');

    console.log(employeeOleksandr);
    console.log(employeeVasyl);
    console.log(employeeRoman);
    console.log(employeeOleksandr.salary);
    console.log(employeeVasyl.salary);
    console.log(employeeRoman.salary);








  
 
    
